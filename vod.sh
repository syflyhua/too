#!/bin/bash

cd `dirname $0`
shubresult=`./shub -url $1`
#shubresult=`./shub -bt FD4A8BB904572FB043B3447DBE46CE122A2F2CA4 4`

cid=`echo $shubresult | awk '{print $1}'`
filesize=`echo $shubresult | awk '{print $2}'`
gcid=`echo $shubresult | awk '{print $3}'`

if [ x"" == x"$filesize" ]
then
   echo "parse shub fail"
   exit 1
fi

echo "http://127.0.0.1:8080/0/$gcid/$cid/$filesize/0/0/0/0/0/0/0/vod.flv"
